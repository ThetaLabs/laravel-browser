<?php

namespace ThetaLabs\Browser\Facades;

use Illuminate\Support\Facades\Facade;

class Browser extends Facade
{
    protected static function getFacadeAccessor() {
        return 'browser';
    }
}
