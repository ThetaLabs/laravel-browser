<?php

namespace ThetaLabs\Browser;

use Illuminate\Support\ServiceProvider;

class BrowserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('browser', function () {
            return new BrowserManager();
        });
    }

    public function provides()
    {
        return 'browser';
    }

    public function register()
    {
        
    }
}