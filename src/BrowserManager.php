<?php

namespace ThetaLabs\Browser;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Collection;
use ReflectionFunction;
use ThetaLabs\Browser\Chrome\SupportsChrome;

class BrowserManager
{
    use SupportsChrome;

    /**
     * All of the active browser instances.
     *
     * @var Collection
     */
    protected $browsers = [];

    /**
     * Base URL of all browser instances managed
     *
     * @var null|string
     */
    protected $baseUrl = null;

    /**
     * Automatically start the browser instance
     *
     * @var bool
     */
    protected $autoStart = false;
    
    /**
     * Whether to run in headless mode
     *
     * @var bool
     */
    protected $headless = false;

    /**
     * Driver create callback
     *
     * @var callable
     */
    protected $createDriverCallback = null;

    /**
     * The callbacks that should be run on class tear down.
     *
     * @var array
     */
    protected static $afterClassCallbacks = [];

    /**
     * Register the base URL with Dusk.
     */
    public function __construct()
    {
        Browser::$storeScreenshotsAt = storage_path('browser/screenshots');
        Browser::$storeConsoleLogAt = storage_path('browser/console');
    }

    /**
     * Register an "after class" tear down callback.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function afterClass(\Closure $callback)
    {
        static::$afterClassCallbacks[] = $callback;
    }

    public function __destruct()
    {
        self::closeAll();

        foreach (static::$afterClassCallbacks as $callback) {
            $callback();
        }
    }

    /**
     * Create a new browser instance.
     *
     * @param  \Closure  $callback
     * @return \ThetaLabs\Browser\Browser|void
     * @throws \Exception
     * @throws \Throwable
     */
    public function browse(\Closure $callback)
    {
        $browsers = $this->createBrowsersFor($callback);
        try {
            $callback(...$browsers->all());
        } catch (\Exception $e) {
            $this->captureFailuresFor($browsers);
            throw $e;
        } catch (\Throwable $e) {
            $this->captureFailuresFor($browsers);
            throw $e;
        } finally {
            $this->storeConsoleLogsFor($browsers);
            $this->closeAll();
//            $this->browsers = $this->closeAllButPrimary($browsers);
        }
    }
    /**
     * Create the browser instances needed for the given callback.
     *
     * @param  \Closure  $callback
     * @return Collection
     */
    protected function createBrowsersFor(\Closure $callback)
    {
        if (count($this->browsers) === 0) {
            $this->browsers = collect([$this->newBrowser($this->createWebDriver())]);
        }
        $additional = $this->browsersNeededFor($callback) - 1;
        for ($i = 0; $i < $additional; $i++) {
            $this->browsers->push($this->newBrowser($this->createWebDriver()));
        }
        return $this->browsers;
    }
    /**
     * Create a new Browser instance.
     *
     * @param  \Facebook\WebDriver\Remote\RemoteWebDriver  $driver
     * @return \ThetaLabs\Browser\Browser
     */
    protected function newBrowser($driver)
    {
        $browser = new Browser($driver);
        $browser->baseUrl = $this->baseUrl();

        return $browser;
    }
    /**
     * Get the number of browsers needed for a given callback.
     *
     * @param  \Closure  $callback
     * @return int
     */
    protected function browsersNeededFor(\Closure $callback)
    {
        return (new ReflectionFunction($callback))->getNumberOfParameters();
    }
    /**
     * Capture failure screenshots for each browser.
     *
     * @param  \Illuminate\Support\Collection  $browsers
     * @return void
     */
    protected function captureFailuresFor($browsers)
    {
        $browsers->each(function (Browser $browser, $key) {
            $browser->screenshot('failure-'.$this->getName($browser).'-'.$key);
        });
    }
    /**
     * Store the console output for the given browsers.
     *
     * @param  \Illuminate\Support\Collection  $browsers
     * @return void
     */
    protected function storeConsoleLogsFor($browsers)
    {
        $browsers->each(function (Browser $browser, $key) {
            $browser->storeConsoleLog($this->getName($browser).'-'.$key);
        });
    }
    /**
     * Close all of the browsers except the primary (first) one.
     *
     * @param  \Illuminate\Support\Collection  $browsers
     * @return \Illuminate\Support\Collection
     */
    protected function closeAllButPrimary($browsers)
    {
        $browsers->slice(1)->each(function ($browser) {
            $browser->quit();
        });

        return $browsers->take(1);
    }
    /**
     * Close all of the active browsers.
     *
     * @return void
     */
    public function closeAll()
    {
        Collection::make($this->browsers)->each(function ($browser) {
            $browser->quit();
        });

        $this->browsers = collect();

        if ($this->autoStart) {
            static::stopChromeDriver();
        }
    }
    /**
     * Create the remote web driver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function createWebDriver()
    {
        return retry(5, function () {
            return $this->driver();
        }, 50);
    }

    /**
     * Overrides the createDriver functionality
     *
     * @param callable $callback
     */
    public function setCreateDriver(callable $callback)
    {
        $this->createDriverCallback = $callback;
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        if (is_callable($this->createDriverCallback)) {
            return call_user_func($this->createDriverCallback);
        }

        $options = new ChromeOptions();

        // If in production, use headless mode by default with a
        // standard window size to avoid issues with responsive sites
        if ($this->headless) {
            $options->addArguments([
                '--disable-gpu',
                '--headless',
                '--no-sandbox',
                '--window-size=1920, 1080',
            ]);
        }

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

        if ($this->autoStart) {
            static::startChromeDriver();
        }

        return RemoteWebDriver::create(
            'http://localhost:9515', $capabilities, 60000, 60000
        );
    }

    /**
     * Determine the application's base URL.
     *
     * @var string
     * @return string|null
     */
    protected function baseUrl($url = null)
    {
        if (func_num_args() > 0) {
            $this->baseUrl = $url;
        }

        return $this->baseUrl;
    }

    public function autoStart($flag)
    {
        $this->autoStart = $flag;
    }

    /**
     * Get the name of the browser instance
     *
     * @param Browser $browser
     * @return mixed
     */
    protected function getName(Browser $browser)
    {
        return isset($browser->page) ? $browser->page->url() : $this->baseUrl();
    }

    public function setHeadless($headless = NULL)
    {
        if (is_null($headless)) {
            $this->headless = (getenv('APP_ENV') == 'production');
        } else {
            $this->headless = $headless;
        }
    }
}